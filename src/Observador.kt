package ch4.observer

import java.util.*

class Bebida(val tipo: String)

class BartenderLicoreria : Observable() {

    val nombreLicoreria = "Bandidos del paramo"

    fun prepararMichelada(tipo : String){
        var bebida = Bebida(tipo)

        //este metodo permite conocer cuando existan los cambios
        setChanged()
        notifyObservers(bebida) //Da aviso a los observadores sobre el cambio en tal variable
    }
}

class MeseroLicoreria : Observer{

    val nombreMesero = "Brix"

    override fun update(observado: Observable?, arg: Any?) {
        when (observado){
            is BartenderLicoreria -> {
                if (arg is Bebida){
                    println("$nombreMesero esta llevándole la bebida que eligió la cul es:  ${arg.tipo} ,preparada por:" +
                            " ${observado.nombreLicoreria}")
                }
            }
            else -> println(observado?.javaClass.toString())
        }
    }
}

fun main(args : Array<String>){
    val bob = BartenderLicoreria()
    bob.addObserver(MeseroLicoreria())

    bob.prepararMichelada("Michelada de Maracuya y Mandarina")
}